#include<stdio.h>
int main()
{
    char c;
    FILE *fp;
    fp=fopen("INPUT.txt","w");
    printf("Enter input data:\n");
    while((c=getchar())!=EOF)
    {
        fputc(c,fp);
    }
    fclose(fp);
    fp=fopen("INPUT.txt","r");
    printf("The contents of the file are:\n");
    while((c=fgetc(fp))!=EOF)
    {
       putchar(c);
    }
    fclose(fp);
    return 0;
}