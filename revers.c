#include<stdio.h>
int input()
{
    int num;
    printf("Enter a no:\n");
    scanf("%d",&num);
    return num;
}
int compute()
{
    int num,rev,temp;
    rev=0;
    while(num!=0)
    {
        temp=num%10;
        rev=rev*10+temp;
        num=num/10;
    }
    return rev;
}    
int main()
{
    int num,rev,temp;
    num=input();
    rev=compute();
    printf("Reversed num=%d",rev);
    return 0;
}
