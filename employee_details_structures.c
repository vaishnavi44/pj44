#include<stdio.h>
struct date
{
    int day;
    char month[20];
    int year;
};
struct Employee
{
    int id;
    char name[20];
    float sal;
    struct date DOJ;
};
int main()
{
    struct Employee E1;
    printf("Enter employee Details\n");
    printf("Enter employee ID\n");
    scanf("%d",&E1.id);
    printf("Enter employee name\n");
    scanf("%s",E1.name);
    printf("Enter employee sal\n");
    scanf("%f",&E1.sal);
    printf("Enter date of join\n");
    printf("Enter day,month,year\n");
    scanf("%d%s%d",&E1.DOJ.day,E1.DOJ.month,&E1.DOJ.year);
    printf("Employee details are \nID:%d\nName:%s\nSalary:%f\nDate of join:%d %s %d",E1.id,E1.name,E1.sal,E1.DOJ.day,E1.DOJ.month,E1.DOJ.year);
    return 0;
}