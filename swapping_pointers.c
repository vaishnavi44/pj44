#include<stdio.h>
void swap(int *x,int *y)
{
    int t;
    t=*x;
    *x=*y;
    *y=t;
}
int main()
{
    int a,b;
    printf("Enter the values of a and b:\n");
    scanf("%d%d",&a,&b);
    swap(&a,&b);
    printf("The values of a and b after swapping:\na:%d\nb:%d\n",a,b);
    return 0;
}
