#include<stdio.h>
int input()
{
    int n;
    printf("Enter a number:\n");
    scanf("%d",&n);
    return n;
}
int compute(int n)
{
    int x, i=2;
    if (n%2 == 0)
        x = n/2;
    else
        x = (n+1)/2;
    while(i<=x)
    {
        if (n%i == 0)
            return 0;
        else 
            return 1;
		i += 1;
    }
}
void output(int n,int f)
{
    int y;
    y = compute(n);
    if (y == 0)
        printf("The number is composite\n");
    else if (y == 1)
        printf("The number is prime\n");
}
int main()
{
    int n, f;
    n = input();
    f = compute(n);
    output(n,f);
    return 0;
}

          