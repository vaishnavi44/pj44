#include<stdio.h>
#include<math.h>
struct point
{
	float x,y;
};
struct gets
{
	float l;
	float m;
	struct point p,p1,p2;
};
typedef struct gets info;
int numbers()
{
	int num;
	printf("Enter how many line segments:\n");
	scanf("%d",&num);
	return num;
}
void getarray(int n,info arr[])
{
	for(int i=0;i<n;i++)
	{
		printf("Enter the length,slope and leftends points of %d line\n",i+1);
	    scanf("%f%f%f%f",&arr[i].l,&arr[i].m,&arr[i].p2.x,&arr[i].p2.y);
	}
}
void compute(int n,info arr[])
{
	for(int i=0;i<n;i++)
	{
		arr[i].p.x=sqrt(pow(arr[i].l,2)/(1+pow(arr[i].m,2)));
		arr[i].p.y=arr[i].m*arr[i].p.x;
		arr[i].p1.x=arr[i].p2.x+arr[i].p.x;
		arr[i].p1.y=arr[i].p2.y+arr[i].p.y;
	}
}
void output(int n,info arr[])
{
	for(int i=0;i<n;i++)
	{
		printf("%d line segment Length=%f\nSlop=%f\nP=(%f,%f) and Q=(%f,%f)\n",i+1,arr[i].l,arr[i].m,arr[i].p2.x,arr[i].p2.y,arr[i].p1.x,arr[i].p1.y);
	}

}
int main()
{
	int n;
	n=numbers();
	info arr[n];
	getarray(n,arr);
	compute(n,arr);
	output(n,arr);
	return 0;
}