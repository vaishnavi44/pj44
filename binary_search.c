#include<stdio.h>
int main()
{
    int n,key;
    printf("Enter the length of the array:\n");
    scanf("%d",&n);
    int arr[n];
    printf("Enter the elements in ascending order:\n");
    for(int i=0;i<n;i++)
    {
        scanf("%d",&arr[i]);
    }
    printf("Enter the value to be searched:\n");
    scanf("%d",&key);
    int beg=0,mid,end=n-1,flag;
    while(beg<=end)
    {
        mid=(beg+end)/2;
        if(arr[mid]==key)
        {
            flag=1;
            break;
        }
        else if(arr[mid]<key)
        { 
            mid=beg+1;
            
        }
        else
        {
            mid=end-1;
            
        }
    }    
    if(flag==1)
        printf("The number is found in the position%d",mid);
    else
        printf("Invalid Input\n");
    return 0;
}