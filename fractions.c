#include<stdio.h>
struct fraction
{
    int n;
    int d;
};
typedef struct fraction Frac;
Frac input()
{
    Frac f;
    printf("Enter the numerator and denominator of the number\n");
    scanf("%d%d",&f.n,&f.d);
    return f;
}
Frac compute(Frac f1,Frac f2)
{
	Frac sum;
    sum.n=f1.n*f2.d+f2.n*f1.d;
    sum.d=f1.d*f2.d;
    return sum;
}
int gcd(int sn,int sd)
{
	int g;
    for(int i=1;i<=sn&&i<=sd;i++)
    {
        if(sn%i==0&&sd%i==0)
        {
            g=i;
        }
    }
	return g;
}
Frac reduce(Frac sum)
{
    int g;
    g=gcd(sum.n,sum.d);
	sum.n=sum.n/g;
	sum.d=sum.d/g;
	return sum;
}
    
void output(Frac sum)
{ 
   
    printf("Sum=%d/%d",sum.n,sum.d);
}
int main()
{
    Frac f1,f2,sum;
	int g,sn,sd;
    f1=input();
    f2=input();
    sum=compute(f1,f2);
	sn=sum.n;
	sd=sum.d;
	g=gcd(sn,sd);
	sum=reduce(sum);
    output(sum);
    return 0;
}