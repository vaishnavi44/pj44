#include <stdio.h>

int input()
{
    int no;
    printf("Enter the no for sum of digits\n");
    scanf("%d",&no);
    return no;
}

int compute(int no)
{
    int d,sum=0;
    while(no>0)
    {
        d=no%10;
        sum=sum+d;
        no=no/10;
    }
    return sum;
}

void output(int sum)
{
    printf("Sum = %d\n",sum);
}

int main()
{
    int no;
    no=input();
    no=compute(no);
    output(no);
    return 0;
}