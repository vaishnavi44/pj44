#include<stdio.h>
struct fraction 
{  
    int n;
    int d;
};
typedef struct fraction Frac;
Frac input()
{ 
    Frac f;
    printf("enter the numerator and denominator\n");
    scanf("%d%d",&f.n,&f.d);
    return f;
}
int numbers()
{
    int num;
    printf("How many numbers\n");
    scanf("%d",&num);
    return num;
}
void getarray(int n,Frac arr[])
{
    
    for(int i=0;i<n;i++)
    {
        printf("enter the numerator and denominator of %d number:\n",i);
        scanf("%d%d",&arr[i].n,&arr[i].d);
    }
}
int gcd(int sn,int sd)
{
	int t;
	while(sd!=0)
	{
		t=sd;
		sd=sn%sd;
		sn=t;
	}
	return sn;
}
Frac compute(int n,Frac arr[])
{
    Frac sum;
    sum.n=arr[0].n;
    sum.d=arr[0].d;
    for(int i=1;i<n;i++)
    {
        sum.n=sum.n*arr[i].d+sum.d*arr[i].n;
        sum.d=sum.d*arr[i].d;
    }
	int g;
	g=gcd(sum.n,sum.d);
	sum.n=sum.n/g;
	sum.d=sum.d/g;
    return sum;
}
void output(int n,Frac arr[],Frac sum)
{
	for(int i=0;i<n;i++)
	{
		printf("%d/%d+",arr[i].n,arr[i].d);
	}
	printf("=%d/%d",sum.n,sum.d);
}
int main()
{ 
    int n;
    n=numbers();
    Frac arr[n];
    getarray(n,arr);
    Frac sum;
    sum=compute(n,arr);
    output(n,arr,sum);
    return 0;     
}
       