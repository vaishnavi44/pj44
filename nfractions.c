#include<stdio.h>
struct fraction 
{  
    int n;
    int d;
};
typedef struct fraction Frac;
Frac input()
{ 
    Frac f;
    printf("enter the numerator and denominator\n");
    scanf("%d%d",&f.n,&f.d);
    return f;
}
int numbers()
{
    int num;
    printf("How many numbers\n");
    scanf("%d",&num);
    return num;
}
void getarray(int n,Frac arr[])
{
    
    for(int i=1;i<=n;i++)
    {
        printf("enter the numerator and denominator of %d number:\n",i);
        scanf("%d%d",&arr[i].n,&arr[i].d);
    }
}
Frac compute(Frac arr[], int n)
{
    Frac sum;
    sum.n=arr[1].n;
    sum.d=arr[1].d;
    for(int i=2;i<=n;i++)
    {
        sum.n=sum.n*arr[i].d+sum.d*arr[i].n;
        sum.d=sum.d*arr[i].d;
    }
    return sum;
}
int gcd(int sn,int sd)
{
	int g;
	for(int i=1;i<=sn&&i<=sd;i++)
	{
		if(sn%i==0&&sd%i==0)
		{
		    g=i;	
		}
		
	}
	return g;
}
Frac reduce(int g,Frac sum)
{
	sum.n=sum.n/g;
	sum.d=sum.d/g;
	return sum;
}
void output(Frac sum)
{
    int gcd;
    for(int i=1;i<=sum.n&&i<=sum.d;i++)
    {
        if(sum.n%i==0&&sum.d%i==0)
        {
            gcd=i;
        }
    }
    sum.n=sum.n/gcd;
    sum.d=sum.d/gcd;
    printf("Sum=%d/%d",sum.n,sum.d);
}
int main()
{ 
    int n;
    n=numbers();
    Frac arr[n];
    getarray(n,arr);
    Frac sum;
    sum=compute(arr,n);
	int sn=sum.n;
	int sd=sum.d;
	int g;
	g=gcd(sn,sd);
	sum=reduce(g,sum);
    output(sum);
    return 0;     
}
        
